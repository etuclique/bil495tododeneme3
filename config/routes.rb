Rails.application.routes.draw do

resources :users do
  resources :issues
end 

get '/welcome' => 'api/v1/issues#welcome'
match '/create/:user_id/issue', to: 'api/v1/issues#create', via: [:get ,:post ], as: 'create'

match '/index/user/:user_id' => 'api/v1/issues#index', via: [:get, :post, :destroy,:delete, :patch],  as:'index'

post 'api/v1/users/:user_id/issues/:id' => 'api/v1/issues#destroy' , as:'delete'
match '/update/:user_id/issue/:id', to: 'api/v1/issues#update', via: [:get ,:post ,:patch], as: 'update'
get '/show/user/:user_id/issue/:id' , to: 'api/v1/issues#show', as: 'show'

match '/complete/user/:user_id/issue/:id', to: 'api/v1/issues#complete', via: [:get ,:post ], as: 'complete'

get 'buhafta/user/:user_id' => 'api/v1/issues#buHafta'
match 'gelecekzaman/user/:user_id' => 'api/v1/issues#gelecekZaman', via: [:get, :post, :destroy,:delete , :patch],  as:'gelecekZaman'
get 'suresigecenler/user/:user_id' => 'api/v1/issues#suresiGecenler'
get 'tamamlananlar/user/:user_id' => 'api/v1/issues#tamamlananlar'

root 'api/v1/issues#index', :user_id => '1'
#match '/api/v1/users/:user_id/issues' => 'api/v1/issues#new', via: :post

  # API routes path
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      #devise_for :users
      resources :users, :only => [:show, :create, :update, :destroy,:index] do
        resources :issues, :only => [:show, :create, :update, :destroy,:index] do
          collection do
            get 'buHafta' 
            get 'gelecekZaman'
            get 'suresiGecenler'
            get 'tamamlananlar'
          end
        end
      end
    end
  end  

 
end


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

