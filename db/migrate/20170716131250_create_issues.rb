class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.string :title
      t.string :content
      t.string :dueDate
      t.boolean :isItDone
      t.boolean :overDue

      t.timestamps null: false
    end
  end
end
