class Api::V1::IssuesController < ApplicationController
  respond_to :json, :html
  skip_before_filter  :verify_authenticity_token
  
 
  def welcome
  end
  def buHafta
    user = User.find(params[:user_id])
    respond_to do |format|
      format.html { 
        respond_with(@issuesthisweek= Issue.buHaftaki(user),@userid= user.id ,@issuescounttoday= Issue.bugunkuleriSay(user), @issuescountolds= Issue.buGundenOncekileriSay(user),@userid= user.id, @issuescountcompleted= Issue.tamamlanmislariSay(user),@userid= user.id,@issuescountthisweek= Issue.buHaftakileriSay(user),@userid= user.id,@issuescountnextweeks= Issue.buHaftadanSonrakileriSay(user),@userid= user.id)
       
      }
      format.json{ respond_with Issue.buHaftaki(user) }
    end
  end

  def gelecekZaman
    user = User.find(params[:user_id])
    respond_to do |format|
      format.html { respond_with(@issuesnexttime = Issue.buHaftadanSonra(user),@userid= user.id,@issuescounttoday= Issue.bugunkuleriSay(user),  @issuescountolds= Issue.buGundenOncekileriSay(user),@userid= user.id, @issuescountcompleted= Issue.tamamlanmislariSay(user),@userid= user.id,@issuescountthisweek= Issue.buHaftakileriSay(user),@userid= user.id,@issuescountnextweeks= Issue.buHaftadanSonrakileriSay(user),@userid= user.id)
      }
     
      format.json{ respond_with Issue.buHaftadanSonra(user)}
    end
  end
def suresiGecenler
    user = User.find(params[:user_id])
    respond_to do |format|
      format.html { respond_with(@issuestimeout= Issue.buGundenOnceki(user) ,@issuescounttoday= Issue.bugunkuleriSay(user), @issuescountolds= Issue.buGundenOncekileriSay(user),@userid= user.id, @issuescountcompleted= Issue.tamamlanmislariSay(user),@userid= user.id,@issuescountthisweek= Issue.buHaftakileriSay(user),@userid= user.id,@issuescountnextweeks= Issue.buHaftadanSonrakileriSay(user),@userid= user.id)}
     
      format.json{ respond_with Issue.buGundenOnceki(user)}
    end
end

def complete
   user = User.find(params[:user_id])
   issue = user.issues.find(params[:id])
   issue.isItDone = true
   issue.save
  respond_to do |format|
        format.html { redirect_to :back }
        format.json { render json: issue, status: 200 }
    end
end

def tamamlananlar
    user = User.find(params[:user_id])
    respond_to do |format|
      format.html { respond_with(@issuescompleted= Issue.tamamlanmislar(user),@issuescounttoday= Issue.bugunkuleriSay(user), @issuescountolds= Issue.buGundenOncekileriSay(user),@userid= user.id, @issuescountcompleted= Issue.tamamlanmislariSay(user),@userid= user.id,@issuescountthisweek= Issue.buHaftakileriSay(user),@userid= user.id,@issuescountnextweeks= Issue.buHaftadanSonrakileriSay(user),@userid= user.id)}
     
      format.json{ respond_with Issue.tamamlanmislar(user)}
    end
end
  
  def index
    user = User.find(params[:user_id])
    respond_to do |format|
      format.html { respond_with(@userid= user.id,@issuestoday= Issue.bugunku(user),@issuescounttoday= Issue.bugunkuleriSay(user), @issuescountolds= Issue.buGundenOncekileriSay(user),@userid= user.id, @issuescountcompleted= Issue.tamamlanmislariSay(user),@userid= user.id,@issuescountthisweek= Issue.buHaftakileriSay(user),@userid= user.id,@issuescountnextweeks= Issue.buHaftadanSonrakileriSay(user),@userid= user.id) }
      format.json { respond_with user.issues }
    end
   
  end


  def show
    user = User.find(params[:user_id])
    respond_to do |format|
      format.html { respond_with(@issue=user.issues.find(params[:id]))}
      format.json { respond_with user.issues.find(params[:id]) }
    end
  end
 
  def create
    user = User.find(params[:user_id])
    @issue=user.issues.new(issue_params)
    respond_to do |format|
      if @issue.save
        
        format.html { }
        format.json { render json: @issue, status: 201 }
      else
        format.html {redirect_to buHafta_issues_v1_api_url}
        format.json { render json: { errors: @issue.errors}, status: 422 }
      end
    end
  end
  
  def update
    user = User.find(params[:user_id])
    @issue = user.issues.find(params[:id])
    respond_to do |format|
      if @issue.update(issue_update_params)
        format.html { flash.keep[:notice]="This message will persist" }
        format.json { render json: @issue, status: 200 }
      else
        format.html { render action: "update"  }
        format.json { render json: { errors: @issue.errors }, status: 422 }
      end
    end
  end

  # Deleting users
  def destroy
    user = User.find(params[:user_id])
    issue = user.issues.find(params[:id])
    issue.destroy!
    
    respond_to do |format|
      format.html { redirect_to :back}
      format.json { head :no_content }
    end
  end
 
  def issue_params
  #params.require(:issue).permit(:title, :content, :dueDate)
  params.permit(:id,:title, :content, :dueDate)
  end
  
  def issue_update_params
  #params.require(:issue).permit(:title, :content, :dueDate)
   params.permit(:title, :content, :dueDate, :isItDone)
  end
  
end