class Issue < ActiveRecord::Base
    belongs_to :user
    
    def self.bugunku(user)
        return user.issues.where(dueDate: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day)
    end
    def self.buHaftaki(user)
        return user.issues.where(dueDate: Date.today..1.week.from_now,isItDone: nil && false)
    end
    
    
    def self.buHaftadanSonra(user)
        return user.issues.where("\"dueDate\" > ? AND \"isItDone\" IS NULL OR \"isItDone\" = ?",1.week.from_now, false)
    end
    
    def self.buGundenOnceki(user)
        return user.issues.where("\"dueDate\" < ?", Date.today)
    end
    
    def self.tamamlanmislar(user)
        return user.issues.where("\"isItDone\" = ?", true)
    end

    def self.bugunkuleriSay(user)
        return user.issues.where(dueDate: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day).count()
    end
    def self.buHaftakileriSay(user)
        return user.issues.where(dueDate: Date.today..1.week.from_now,isItDone: nil && false).count()
    end
    def self.buHaftadanSonrakileriSay(user)
        return user.issues.where("\"dueDate\" > ? AND \"isItDone\" IS NULL OR \"isItDone\" = ?",1.week.from_now, false).count()
    end
    def self.buGundenOncekileriSay(user)
        return user.issues.where("\"dueDate\" < ?", Date.today).count()
    end
    def self.tamamlanmislariSay(user)
        return user.issues.where("\"isItDone\" = ?", true).count()
    end
    
end
